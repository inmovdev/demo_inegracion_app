import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button, Platform, Alert, Image, Dimensions, Linking } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import ImageAutoheight from './components/ImageAutoheight';
import * as Location from 'expo-location';
import { Camera } from 'expo-camera';
import { useRef } from 'react';


const containerWidth = Math.min(Dimensions.get('screen').width - 20, 600);

export default function App() {
  const [location, setLocation] = useState<any>(null);
  const [hasCameraAccess, setCameraAccess] = useState(false);
  const cameraRef = useRef<Camera>(null);
  const [id, setId] = useState<any>(1);
  const [uploaded, setUploaded] = useState<any>([]);
  const [hasPermissions, setHasPermissions] = React.useState(false);
  

  const _getImageBlob = useCallback(async (uri: string): Promise<Blob> => {
    const blob: Blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        console.log(e);
        reject(new TypeError("Network request failed"));
      };
      xhr.responseType = "blob";
      xhr.open("GET", uri, true);
      xhr.send(null);
    });
    return blob;
  }, [])

  const _uploadImage = useCallback(async (uri: string) => {
    const image: Blob = await _getImageBlob(uri);
    const fd = new FormData();
    fd.append('image', image);
    fd.append('external_id', id);
    fd.append('lat', location.latitude);
    fd.append('lon', location.longitude);

    const res = await fetch('https://api.stagingsv.com/api/demo/image/upload', {
      body: fd,
      method: 'post'
    })

    if (!res.ok) {
      Alert.alert("Error conectando con el servidor");
      return;
    }
    const {data} = await res.json();
    setUploaded(data);
  }, [location, id])

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Lo sentimos los permisos de acceso a tu galería para utilizar la aplicación');
        }
      } else {
        const urlParams = new URLSearchParams(window.location.search);
        const id = urlParams.get('id');

        if (!id) {
          alert('No has especificado un identificador');
          return;
        }
        setId(id);
      }
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        alert('Lo sentimos los permisos de ubicación son requeridos para utilizar la aplicación');
        return;
      }
  
      let location = await Location.getCurrentPositionAsync({
        accuracy: Location.Accuracy.Highest,
      });
  
      setLocation(location.coords);
      setHasPermissions(true)

      const { status: statusCamera } = await Camera.requestPermissionsAsync();
      if (statusCamera !== 'granted') {
        return;
      }
      setCameraAccess(true);
    })();

  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      _uploadImage(result.uri)
    }
  };

  const takePhoto = async () => {
    if (cameraRef.current) {
      const {uri} = await cameraRef.current.takePictureAsync();
      _uploadImage(uri);
    }
  }


  return (
    <View style={styles.container}>
      {uploaded.length == 0 && (
        <View style={{
          width: '100%',
          justifyContent: 'center'
        }}>
          <Text style={{
            textAlign: 'center',
            fontWeight: 'bold',
            fontSize: 30,
            marginBottom: 20,
          }}>Sube tu fotografía</Text>
          <Button color="#03738b" disabled={!hasPermissions} title={!hasPermissions ? 'Esperando permisos...' :'Desde tu galeria'} onPress={pickImage} />
          <Text style={{
            textAlign: 'center',
            fontWeight: 'bold',
            fontSize: 30,
            marginBottom: 20,
            marginTop: 30
          }}>Toma una fotografía</Text>
          {!hasCameraAccess ? <Text style={{
            color: 'gold',
            marginHorizontal: 20,
            textAlign: 'center',
            fontSize: 20
          }}>Debes aceptar los permisos para poder acceder a la cámara</Text> : <>
            <View style={{
            alignSelf: 'center',
            marginBottom: 10,
            width: (containerWidth / 2),
            height: containerWidth / 1.6
          }}>
            <Camera style={{
              flex: 1
            }}
            ref={cameraRef}
            type={Camera.Constants.Type.back}
            >

            </Camera>
          </View>
            <Button color="#03738b" disabled={!hasPermissions || !hasCameraAccess} title={!hasPermissions ? 'Esperando permisos...' :'Tomar una fotografía'} onPress={takePhoto} />
          </>}
        </View>
      )}

      <View
      style={{
        marginTop: 20,
        width: containerWidth,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
      }}>
        {uploaded.map((upload: any) => (
          <View
          key={String(upload.id)}
          style={{
            borderWidth: 1,
            borderColor: '#ccc',
            marginBottom: 20,
            width: containerWidth,
            justifyContent: 'center'
          }}>
                <ImageAutoheight
                  width={containerWidth - 4}
                  uri={upload.image}
                />
                <View style={{
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  marginBottom: 40
                }}>
                  <View style={{
                    marginBottom: 10
                  }}>
                    <Text style={{
                      fontWeight: 'bold'
                    }}>Subido por:</Text>
                    <Text>{upload.external_id}</Text>
                  </View>
                  <Button color="#03738b" title="Rastrear registro" onPress={() => {
                      Linking.openURL(`http://www.google.com/maps/place/${upload.lat},${upload.lon}`)
                    }}></Button>
                </View>
          </View>
        ))}
      </View>
    </View>)
    }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    maxWidth: containerWidth,
    margin: 'auto',
    alignContent: 'center',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
