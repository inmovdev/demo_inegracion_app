import * as React from 'react';
import { Image } from 'react-native';

export interface ImageAutoheightProps {
  uri: string,
  width: number,
}
 
const ImageAutoheight: React.FunctionComponent<ImageAutoheightProps> = ({ uri, width }: ImageAutoheightProps) => {
  const [aspect, setAspect] = React.useState(0);

  React.useEffect(() => {
    Image.getSize(uri, (width, height) => {
      console.log(width / height)
      setAspect(width / height);
    });
  }, [uri])

  return (
    <Image
      style={{
        width: width,
        minHeight: width * 0.4,
        height: width / aspect,
        resizeMode: 'cover',
        marginHorizontal: '5'
      }}
      source={{
        uri,
      }}
    ></Image>
  );
}
 
export default ImageAutoheight;